####Introduction
- RabbitMQ is a message broker
- it accepts and forwards messages.
- RabbitMQ is a like post box, a post office and a postman
- it accepts, stores and forwards binary blobs of data ‒ messages. 

#### Terms
- Producing: sending a message 
- producer: program that sends messages
- queue: 
    - like a post box which lives inside RabbitMQ
    - stored message inside a queue
    - bound by the host's memory & disk limits
    
- Consuming
    - meaning to receiving
 
- consumer
    - waits to receive messages
    
- exchange
    - allows exactly to which queue the message should go.
    
- routing_key
    - queue name specified here 

- callback:
    - Whenever we receive a message, 
    - the callback function is called by the Pika library
    
- auto_ack
    - 

    
    
- Note
    - producer, consumer, and broker do not have to reside on the same host
    - application can be both a producer and consumer 
    
    
#### install 

- rabbit mq
- https://computingforgeeks.com/how-to-install-latest-erlang-on-ubuntu-18-04-lts/
- https://computingforgeeks.com/how-to-install-latest-rabbitmq-server-on-ubuntu-18-04-lts/


##### RabbitMQ libraries
    
    - pip install pika --upgrade
    
#### using the Pika Python client

#### how to send and receive a message from a named queue

- programme 
    - producer (sender) that sends a single message
    - consumer (receiver) that receives messages
    - prints "Hello World" of messaging.
    
- "P" is our producer 
- "C" is our consumer. 
-  box in red is a queue (named hello)

- Producer sends messages to the "hello" queue. 
- The consumer receives messages from that "hello" queue.

- send.py
    - single message to the queue
    - establish a connection with RabbitMQ server (broker)
    - specify name or IP address to connect to a broker on a different 
    machine .
    
     
    #!/usr/bin/env python
    import pika
    
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    
   - check recipient queue exists
   - RabbitMQ drop the message if location not exists 
   
   - create a hello queue
   
    channel.queue_declare(queue='hello')
    
   - send string Hello World! to hello queue.
    
    - message can't be sent directly to queue
    - message first go through exchange
    - default exchange identified by an empty string.
    - exchange
        - allows exactly to which queue the message should go.
    - routing_key: queue name specified here 
    
    
    
  - code 
  
    
    channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='Hello World!')
    print(" [x] Sent 'Hello World!'")
    
    
  - flush, buffers and 
  - check message was actually delivered to RabbitMQ 
  
  
    connection.close() 
    
    
  
#### Receiving

- receive.py
    - receive messages from the queue
    - print them on the screen
    
    - steps  
    - connect to RabbitMQ server.
    - make sure that the queue exists
    
    - Creating a queue
    
            - queue_declare
        
    - we can run the command as many times
    - only one will be created.
    - if we were sure that queue already exists then We could avoid that .
    - we're not yet sure which program to run first
    - good practice
    
        - channel.queue_declare(queue='hello')
        
    - Receiving messages from the queue works by subscribing a callback function to a queue
    - Whenever we receive a message, this callback function is called by the Pika library
    
    
    - 
    def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    
   
   - tell RabbitMQ that this particular callback function should receive messages 
   - from our hello queue:
   
   
    - channel.basic_consume(queue='hello',
                      auto_ack=True,
                      on_message_callback=callback)            
                             
   - never-ending loop that waits for data and runs callbacks whenever necessary.
   
   
    - print(' [*] Waiting for messages. To exit press CTRL+C')
      channel.start_consuming()
        
    
#### chapter 2
#### Work Queues (Task Queues)

- create a Work Queue 
- distribute time-consuming tasks among multiple workers
- like image resized or pdf files to be rendered

- resource-intensive task schedule to be done later

- encapsulate a task as a message and send it to the queue

-  A worker process 
    - running in the background 
    - will pop the tasks and 
    - eventually execute the job.
    - tasks will be shared between them
    
    
- programe 
    - pretending we're busy by time.sleep()
    - number of dots in the string as its complexity
    - one dot means one second 
    
    - send.py 
        - messages to be sent from the command line 
        
        
        message = ' '.join(sys.argv[1:]) or "Hello World!"
        
        
   - receive.py
    
   - fake, a second for every dot in the message body. 
   - It will pop messages from the queue and perform the task, 
   - so let's call it worker.py:
   
   
    import time

    def callback(ch, method, properties, body):
        print(" [x] Received %r" % body)
        time.sleep(body.count(b'.'))
        print(" [x] Done")
        
        
#### Round-robin dispatching

- easily parallelise work
- can add more workers  
- scale easily


- By default, 
- RabbitMQ will send each message to the next consumer, in sequence. 
- On average every consumer will get the same number of messages. 
- This way of distributing messages is called round-robin.  


#### Message acknowledgment

- An ack(nowledgement) is sent back by the consumer 
- to tell RabbitMQ 
- that a particular message had been received, processed and
 that 
- RabbitMQ is free to delete it.

- be sure that no message is lost, even if the workers occasionally die.

- If a consumer dies (its channel is closed, connection is closed, or TCP connection is lost) 
- without sending an ack,
- RabbitMQ will understand that a message wasn't processed fully and 
- will re-queue it to another consumer. 

- else
- RabbitMQ is free to delete that message 


- There aren't any message timeouts;
- RabbitMQ will redeliver the message when the consumer dies.


    
- Manual message acknowledgments
        
        - auto_ack=True flag
        
- ack by receiver/worker 
    
    
        ch.basic_ack(delivery_tag = method.delivery_tag)
        
        
        def callback(ch, method, properties, body):
            print(" [x] Received %r" % body)
            time.sleep( body.count('.') )
            print(" [x] Done")
            ch.basic_ack(delivery_tag = method.delivery_tag)
            
        channel.basic_consume(queue='hello', on_message_callback=callback)
        
        
- Acknowledgement must be sent on the same channel that received the delivery


- Forgotten acknowledgment


    sudo rabbitmqctl list_queues name messages_ready messages_unacknowledged
    
- Message acknowledgment (page https://www.rabbitmq.com/tutorials/tutorial-two-python.html)

#### Message durability

- if the consumer dies, the task isn't lost due to message ack process 
- tasks will still be lost if RabbitMQ server stops.

- Two things required to make messages aren't lost: 
- mark queue and messages as durable.

- RabbitMQ will never lose our queue
- durable=True

- it won't work as we already "hello" queue which is not durable.
- RabbitMQ doesnt allow you to redefine an existing queue
- 
        
- declare queue with different name i.e task_queue
- applied to both the producer and consumer code
- we sure that task_queue queue won't lost even if RabbitMQ restarts.

    
    - channel.queue_declare(queue='task_queue', durable=True)


- mark our messages as persistent 
    - retaining value for a long time then usual
    - save message to disk
    - persistence guarantees aren't strong,
    - for strong guarantees use publisher confirms.
    - https://www.rabbitmq.com/confirms.html
    
- supplying delivery_mode property with a value 2


   channel.basic_publish(exchange='',
                          routing_key="task_queue",
                          body=message,
                          properties=pika.BasicProperties(
                             delivery_mode = 2, # make message persistent
                          ))
                          
                          
                          
#### Fair dispatch
- when all odd messages are heavy and even messages are light
- one worker will be busy and the other one will do hardly any work


- RabbitMQ doesn't know anything about that and will still dispatch messages evenly.
- 

- because RabbitMQ dispatches message when the message enters the queue.
- It doesn't look at the number of unacknowledged messages for a consumer. 
- It just blindly dispatches every n-th message to the n-th consumer.

- to defeat that
- use the basic.qos method with  prefetch_count=1
- tells RabbitMQ not to give more than one message to a worker at a time.

- dispatch a new message to a worker until it has processed and acknowledged the previous one
- it will dispatch it to the next worker that is not still busy


    channel.basic_qos(prefetch_count=1)
    

- message acknowledgments and prefetch_count you can set up a work 
queue. 
- The durability options let the tasks survive even if RabbitMQ is restarted.
    
  
######################## Publish/Subscribe

- deliver the same message to many consumers. 
- publish/subscribe
- we're going to build a simple logging system

- two programs 
    - the first  emit log messages 
        - published log messages broadcast to all the receivers.
        
    - the second receive and print them.
        - one receiver direct the logs to disk; 
        - another receiver and see the logs on the screen.
        

-# Exchanges

- producer never sends any messages directly to a queue. 
- Actually producer doesn't know if a message  delivered to any queue.
- the producer send messages to an exchange.

- receives messages from producers and it pushes them to queues.
- exchange must know exactly what to do with a message it receives.
    - appended to a particular queue
    - appended to many queues
    - should it get discarded
    - exchange type
        - direct
        - topic
        - headers
        - fanout
        
        
    - channel.exchange_declare(exchange='logs',
                         exchange_type='fanout')
                         
                         
                         
- fanout 
- broadcasts all the messages it receives to all the queues it knows
- routing_key can be ignored 
    
    
- publish to our named exchange instead


    channel.basic_publish(exchange='logs',
                          routing_key='',
                          body=message)
                       

-# Temporary queues

- case for our logger.
- hear all log messages
- interested only in currently flowing messages  not in the old once 

- to solve that we need two things.
- connect to Rabbit we need a fresh, empty queue

    
    result = channel.queue_declare()
    


- once the consumer connection is closed, the queue should be deleted. 
    
    
    result = channel.queue_declare(exclusive=True)
    
    
-# Bindings

- tell the exchange to send messages to our queue


    channel.queue_bind(exchange='logs',
                       queue=result.method.queue)
                       

##### Routing

- subscribe only to a subset of the messages
- direct only critical error messages save disk space
- print all of the log messages on the console.


- binding: the queue is interested in messages from this exchange.


- create a binding with a key
    
    
    channel.queue_bind(exchange=exchange_name,
                       queue=queue_name,
                       routing_key='black')
                       
                       
- binding key 
    - depends on the exchange type. 
    - it can be ignored for fanout 
    
    
-# Direct exchange

- allow filtering messages based on their severity. 
- critical errors, which is writing log messages to the disk

- direct exchange
    - binding key matches the routing key
    
- orange -> Q1
- black or green -> Q2
- all other message will be discarded 



-# Multiple bindings

- bind multiple queues with the same binding key.
- type=direct, binding key, black --> Q1 and Q2


-# Emitting logs

-  Instead of fanout,  direct exchange
- supply the log severity as a routing key


- create an exchange first:
    
    
    channel.exchange_declare(exchange='direct_logs',
                             exchange_type='direct')
                             
- send a message

    
    channel.basic_publish(exchange='direct_logs',
                          routing_key=severity,
                          body=message)
                          
-# Subscribing

- new binding for each severity we're interested in.

    
    result = channel.queue_declare(exclusive=True)
    queue_name = result.method.queue
    
    for severity in severities:
        channel.queue_bind(exchange='direct_logs',
                           queue=queue_name,
                           routing_key=severity)
                           
                           
####  listen for messages based on a pattern.

- fanout: exchange only capable of dummy broadcasting
- direct: selectively receiving the logs
          - it can't do routing based on multiple criteria.
          
- now:
    - which routes logs based on both 
    - severity (info/warn/crit...) and 
    - facility (auth/cron/kern...).
    
- exampl: 
    - errors coming from 'cron' 
    - but also all logs from 'kern'.
    
-# Topic exchange

- similar to direct
    -  a message sent with a particular routing key 
    - will be delivered to all the queues 
    - that are bound with a matching binding key.

- no arbitrary routing_key for Messages sent

-routing_key
    - list of words, delimited by dots
    - example: stock.usd.nyse
           nyse.vmw
           quick.orange.rabbit
    - limit of 255 bytes.
     
-  binding key
    - similar to routing key 
    - two important special 
        - (star) can substitute for exactly one word.
        - (hash) can substitute for zero or more words.
        
    - 
    
- example 
    - send messages describe animals
    - messages sent with a routing key
    - routing key-> three words (two dots)
                    <celerity>.<colour>.<species>
    - created three bindings:
        - Q1 is bound with binding key "*.orange.*"
        - Q1 is interested in all the orange animals.
        
        - Q2 with "*.*.rabbit"
        - "lazy.#".
        
        - Q2 wants to hear everything about rabbits, and everything about 
        lazy animals.
        
        
- if routing key 
    quick.orange.rabbit -> deliver to both queues
    lazy.orange.elephant -> deliver to both queues
    
    quick.orange.fox -> first queue
    lazy.brown.fox -> second
    
    lazy.pink.rabbit -> second but only once 
    quick.brown.fox -> descaded 
    

- if we break our rule 
    - won't match any bindings
    - quick.orange.male.rabbit -> not match     
    - lazy.orange.male.rabbit -> second as start with lazy and #
    
- when a queue is bound with "#

    - receive all the messages, 
    - no routing key required - 
    - like in fanout exchange.

- When special *, #
- like a direct one.




#### Remote procedure call (RPC)

- run a function on a remote computer and wait for the result
- build an RPC system: a client and a scalable RPC server.

- RPC service that returns Fibonacci numbers.


-# Client interface
-  "call()"
    - which sends an RPC request 
    - blocks until the answer is received
    
    
    fibonacci_rpc = FibonacciRpcClient()
    result = fibonacci_rpc.call(4)
    print("fib(4) is %r" % result)
    
    
-# Callback queue
- client sends a request message 
- server replies with a response message.

- client needs to send a 'callback' queue address with the request
- to receive a response
    - client send a 'callback' queue address with the request
    
    
    result = channel.queue_declare(exclusive=True)
    callback_queue = result.method.queue
    
    channel.basic_publish(exchange='',
                          routing_key='rpc_queue',
                          properties=pika.BasicProperties(
                                reply_to = callback_queue,
                                ),
                          body=request) 
                                                 
- Message properties

    - AMQP 0-9-1 protocol predefines a set of 14 properties
    - delivery_mode
        - message as persistent (with a value of 2) 
    - content_type
        - mime-type of the encoding
    - reply_to
        - callback queue
    - correlation_id
        - correlate RPC responses with requests
        
    
    
-# Correlation id
- create a single callback queue per client instead of creating callback queue for every RPC request
- which request the response belongs


- set it to a unique value for every request
-  when we receive a message in the callback queue
- we'll look at this property
- based on that we'll be able to match a response with a request.


- unknown correlation_id
- discard the message


- possibility of a race condition on the server side
- rather then failing with an error, we just ingore 

- when RPC server before sending an acknowledgment message for the request 
- the restarted RPC server will process the request again
- thats why client we must handle the duplicate responses gracefully



####

hello_world

python receive.py
python send.py

######

work_queues

# shell 1
python receive.py

# shell 2
python receive.py


# shell 3
python send.py First message.
python send.py Second message..
python send.py Third message...
python send.py Fourth message....
python send.py Fifth message.....

########

pub_sub

python receive.py > logs_from_rabbit.log

python receive.py

python send.py

######

routing 

python receive.py warning error > logs_from_rabbit.log

python receive.py info warning error

python send.py error "Run. Run. Or it will explode."


#####
topic

python receive.py "#"
python receive.py "kern.*"
python receive.py "*.critical"
python receive.py "kern.*" "*.critical"

python send.py "kern.critical" "A critical kernel error"



######

rpc_dir


python receive.py
python send.py














    
     
        



  
        
        
        
    - 
                           

     





        
        
    
- 

 


    
    
   
   
    
    
     

    



